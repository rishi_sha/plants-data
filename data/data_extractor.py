import pandas as pd
import re


class DataExtractor:
    annual_rate_str = 'State annual {} net generation (MWh)'
    annual_rate_percent = 'State {} generation percent (resource mix)'

    def __init__(self, config):
        self._config = config
        try:
            self.types = dict()
            self.__xls = pd.ExcelFile(config['SOURCE_FILE'])
            # assuming only net generation is needed

            self.__df = pd.read_excel(self.__xls, config['SHEET_NAME'], skiprows=[1], usecols='B,DG:EL', index_col=0)
            self.load()
        except Exception as e:
            print('Unable to read source file: {} error: {}'.format(config['SOURCE_FILE'], e))
            raise e

    def load(self):
        col_names = self.__df.columns
        annual_rate_rx = re.compile(r"^State annual ([A-za-z \/]+) net generation \(MWh\)$")
        annual_percent_rx = re.compile(r"^State ([A-za-z \/]+) generation percent \(resource mix\)$")

        for col in col_names:
            m = annual_rate_rx.match(col)
            if m:
                val = annual_rate_rx.findall(col).__getitem__(0)
                self.types[val.replace(' ', '_').replace('/', '')] = val
        print(self.types)

    def get_data(self, ft, num, by, state=None):
        print('Querying data for state:{} ft: {} by: {} num:{}'.format(state, ft, by, num))
        nft = self.types.get(ft.lower())
        rate_column = self.annual_rate_str.format(nft)
        percent_column = self.annual_rate_percent.format(nft)
        filtered_df = self.__df
        if state is not None and len(state.strip()) > 0:
            filtered_df = self.__df.filter(like=state.upper(), axis=0)

        if by == 'rate':
            filtered_df = filtered_df.sort_values(by=rate_column, ascending=False)
        else:
            filtered_df = filtered_df.sort_values(percent_column, ascending=False)

        selected_df = filtered_df[[rate_column, percent_column]]

        if selected_df.empty:
            return None
        print(selected_df.iloc[:num])
        return selected_df.iloc[:num].to_json(orient="split")

    def is_valid_type(self, ft):
        return ft in self.types


if __name__ == "__main__":
    xls = pd.ExcelFile('egrid2019_data.xlsx')
    df = pd.read_excel(xls, 'ST19')
    print(df.dtypes)
    print(xls.sheet_names)
