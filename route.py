import json

from flask import Flask, render_template, request, jsonify, make_response

from data.data_extractor import DataExtractor

app = Flask(__name__)
app.config.from_object('config')

extractor = DataExtractor(app.config)


@app.errorhandler(400)
def resp_400(error):
    response = jsonify({'message': error.description})
    return render_template("page_400.html"), response, 400

@app.route("/plant/health", methods=["GET"])
def health():
    return make_response(jsonify({'state':'UP'}), 200)

@app.route("/plant/top/<ft>", methods=["GET"])
def get_data(ft):
    if not extractor.is_valid_type(ft):
        resp = make_response("'Invalid ft value. '", 400)
        return resp

    state = None
    if 'state' in request.args:
        state = request.args.get('state')
    n = 10
    if 'n' in request.args:
        n = int(request.args.get('n'))
    by = 'rate'
    if 'by' in request.args:
        by = request.args.get('by')

    result = extractor.get_data(ft, n, by, state)
    if not result:
        return make_response(jsonify({'status': 'error', 'result': 'No result found for the given query.'}), 404)
    return make_response(jsonify({'status': 'success', 'result': json.loads(result)}), 200)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
