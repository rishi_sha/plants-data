# Plants Data

### To Build and deploy
`pip3 install -r requirements.txt`

### To Run

`python3.9 route.py`

### APIs exposed 
<p>
 Health: 

    GET /plant/health

    Response: {"state":"UP"}

 Data:

    GET plant/top/<fuelType>?n=<top n result>&by=<sort by rate or percent>&state=<state codes>
    
    Parameters:
    -- n: number of rows to return. int value
    -- by: sort top n values by rate or percent. values: rate/precent
    -- state: state codes to filter data. values:  'AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'HI',
                                                       'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN',
                                                       'MO', 'MS', 'MT', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH',
                                                       'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA', 'VT',
                                                       'WA', 'WI', 'WV', 'WY'
    --fuelType: fuel type to see the result for. values: 'coal', 'oil', 'gas', 'nuclear', 'biomass', 'hydro', 'wind', 'solar', 
                                                            'geothermal', 'other_fossil', 'other_unknown_purchased_fuel', 'total_nonrenewables', 
                                                            'total_renewables', 'total_nonhydro_renewables', 'total_combustion', 'total_noncombustion'
    Response:
        GET localhost:5000/plant/top/other_unknown_purchased_fuel?n=14&by=percent
        {"result":{"columns":["State annual other unknown/ purchased fuel net generation (MWh)","State other unknown/ purchased fuel generation percent (resource mix)"],"data":[[170705.17,0.017508226],[287160.0,0.010330582],[94445.0,0.0090028542],[1495690.166,0.0061061059]],"index":["HI","MT","ME","FL"]},"status":"success"}

</p>