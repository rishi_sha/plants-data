FROM python:3.9-slim-buster

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

WORKDIR /app

COPY requirements.txt app/requirements.txt
RUN pip3 install -r requirements.txt

COPY plants-data /app
ENTRYPOINT [ "python3" ]
CMD [ "route.py"]